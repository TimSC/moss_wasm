import sys
import os
from lexer import Lexer
from syntax import SyntaxAnalyser
from preprocessor import TokensPreprocess
from generate import Generate

if __name__=="__main__":

	fi = open("../test1.moss", "rt")

	lexOut = []
	lexer = Lexer(lexOut)

	lexer.Parse(fi)

	#Move comments and split statements to their own line
	preprocessed = []
	tokensPreprocess = TokensPreprocess(preprocessed)
	tokensPreprocess.Analyse(lexOut)

	#for tok in preprocessed:
	#	print (tok)

	tree = []
	syntaxAnalyser = SyntaxAnalyser()
	syntaxAnalyser.Analyse(preprocessed, tree)
			
	generator = Generate(open("out.wat", "wt"))
	generator.ProcessTree(tree)


