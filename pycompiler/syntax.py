
class SyntaxAnalyser(object):
	
	def __init__(self):
		pass

	def AttemptEval(self, tokens):

		operators = ['*', '/', '+', '-', '==']

		if len(tokens) == 0:		
			raise ValueError("No tokens to evaluate")

		if len(tokens) == 1:
			#A single token should be a variable or literal value
			literalInt = None
			try:
				literalInt = int(tokens[0])
				return ("literal", "i64", literalInt)
			except ValueError:
				pass

			vari = tokens[0]
			if len(vari) >= 2 and vari[0] == '"' and vari[-1] == '"':			
				return ("literal", "str", vari[1:-1])

			if vari in operators:
				raise ValueError("Expected variable or literal, not operator")
			if vari[0].isnumeric():
				raise ValueError("Variable names cannot start with a number")

			return ("variable", vari)

		#print (tokens)		

		depth = 0
		depthLi = []
		zeroDepOpenBrack = 0
		for i, tok in enumerate(tokens):
			depthLi.append(depth)
			if tok == "(":
				if depth == 0: zeroDepOpenBrack += 1
				depth += 1
			elif tok == ")":
				depth -= 1
				if depth < 0:
					raise ValueError("Unexpected closing bracket")

		if depth != 0:
			raise ValueError("Unmatched brackets")

		#print (zeroDepOpenBrack, depthLi)

		if zeroDepOpenBrack == 1:
			#Check if nested in brackets
			if len(tokens) >= 3 and tokens[0] == '(' and tokens[-1] == ')':
				return self.AttemptEval(tokens[1:-1])

			#Check if we are calling a function
			if len(tokens) >= 4 and tokens[1] == '(' and tokens[-1] == ')':
				funcName = tokens[0]
				if funcName[0].isnumeric():
					raise ValueError("Function name cannot start with a number")
				rawFuncArgs = []
				rawArg = []
				for tok in tokens[2:-1]:
					if tok == ",":
						rawFuncArgs.append(rawArg)
						rawArg = []
					else:
						rawArg.append(tok)
				rawFuncArgs.append(rawArg)

				funcArgs = []
				for rarg in rawFuncArgs:
					funcArgs.append(self.AttemptEval(rarg))
				return ("func", funcName, funcArgs)

		#Check for variable definitions
		colonInd = None
		try:
			colonInd = tokens.index(':')
		except ValueError:
			pass

		if colonInd is not None:
			if colonInd != 1:
				raise ValueError("Colon in unexpected position")
			if len(tokens) < 3:
				raise ValueError("Type not specified")

			variName = tokens[0]
			if variName[0].isnumeric():
				raise ValueError("Variable name cannot start with a number")

			return ("vardef", variName, tokens[2:])

		#Try to split expression by operator
		bestOp = None
		bestIndex = None
		bestPrec = None
		for i, (tok, depth) in enumerate(zip(tokens, depthLi)):
			if depth > 0: continue
			if tok not in operators: continue
			prec = operators.index(tok)
			
			if bestPrec is None or prec < bestPrec:
				bestPrec = prec
				bestIndex = i
				bestOp = tok
		
		if bestOp is None:
			raise SyntaxError("Expression not recognized")

		if bestIndex == 0 or bestIndex == len(tokens)-1:
			raise ValueError("Unexpected operator")

		lhs = tokens[:bestIndex]
		rhs = tokens[bestIndex+1:]

		#print (lhs, rhs)

		lhsEval = self.AttemptEval(lhs)
		rhsEval = self.AttemptEval(rhs)

		return ("operator", bestOp, lhsEval, rhsEval)

	def FunctionBody(self, lexData, statNum, indent):

		funcBody = []

		#Function contents intented by one more level
		while statNum < len(lexData):
		
			flineNum, findent, fchunks = lexData[statNum]

			isComment = len(fchunks) > 0 and len(fchunks[0]) > 0 and fchunks[0][0] == '#'
			hasAssignment = None
			for i, tok in enumerate(fchunks):
				if tok == "=":
					hasAssignment = i

			if findent <= indent and len(fchunks) > 0 and not isComment:
				statNum -= 1
				break

			if len(fchunks) == 0:
				#Empty lines don't end an indent block
				statNum += 1
				continue

			if findent != indent+1 and not isComment:
				raise ValueError("Unexpected indent")

			if isComment:
				funcBody.append(("comment", fchunks[0][1:]))

			elif len(fchunks) > 0 and fchunks[0] == 'return':
				retVal = self.AttemptEval(fchunks[1:])

				funcBody.append(("return", retVal))
				
			elif len(fchunks) > 0 and fchunks[0] == 'if':

				if fchunks[-1] != ':':
					raise ValueError("Missing colon at end of if")

				statNum, ifBody = self.FunctionBody(lexData, statNum+1, findent)

				condition = self.AttemptEval(fchunks[1:-1])

				funcBody.append(("if", condition, ifBody))

			#elif len(fchunks) > 0 and fchunks[0] == 'print':
			#	funcBody.append(("print", fchunks[1:]))

			elif hasAssignment is not None:
				funcBody.append(("assign", 
					self.AttemptEval(fchunks[:hasAssignment]), 
					self.AttemptEval(fchunks[hasAssignment+1:])))

			elif len(fchunks) > 0 and fchunks[0] == 'pass':
				pass

			else:
				evalStat = self.AttemptEval(fchunks)
				funcBody.append(evalStat)

			statNum += 1

		return statNum, funcBody

	def IsType(self, typeStr):
		if typeStr == 'i32': return True
		elif typeStr == 'i64': return True
		return False

	def FunctionDefinition(self, lexData, statNum):

		lineNum, indent, chunks = lexData[statNum]

		if len(chunks) >= 2:
			funcName = chunks[1]
		else:
			raise ValueError("Function name not declared")

		if chunks[2] != '(':
			raise ValueError("Expected function open bracket")

		i = 3
		argsList = []
		while i < len(chunks):

			if chunks[i] == ')':
				i += 1
				break

			args = chunks[i:i+3]
			if len(args) != 3:
				raise ValueError("Function args has unexpected length")

			if args[1] != ":": raise ValueError("Expected arg type semicolon")
			if not self.IsType(args[2]):
				raise ValueError("Unrecognised type {}".format(args[2]))
			argsList.append((args[0], args[2]))
			i += 3

			if chunks[i] == ',':
				i += 1
			elif chunks[i] != ')':
				raise ValueError("Expected , or ) in arg list")
		
		funcEnd = chunks[i:]
		returnType = None
		if len(funcEnd) == 1:
			if funcEnd[0] != ':':
				raise ValueError("Expected : in function declaration")
		
		elif len(funcEnd) == 3:
			if funcEnd[0] != '->':
				raise ValueError("Expected -> in function declaration")

			returnType = funcEnd[1]

			if funcEnd[2] != ':':
				raise ValueError("Expected : in function declaration")
			if not self.IsType(returnType):
				raise ValueError("Unrecognised type {}".format(returnType))

		else:
			raise ValueError("Expect end of function declaration")

		statNum, funcBody = self.FunctionBody(lexData, statNum+1, indent)

		funcdecl = ("funcdecl", funcName, argsList, returnType, funcBody)
		return statNum, funcdecl

	def Analyse(self, lexData, tree):

		statNum = 0
		
		while statNum < len(lexData):

			lineNum, indent, chunks = lexData[statNum]
			#print (lineNum, indent, chunks)

			if len(chunks) == 0:
				statNum += 1
				continue

			isComment = len(chunks) > 0 and len(chunks[0]) > 0 and chunks[0][0] == '#'

			#Function definition
			if chunks[0] == 'def':
				statNum, funcdecl = self.FunctionDefinition(lexData, statNum)
				tree.append(funcdecl)

			elif chunks[0] == 'pass':
				pass
			elif isComment:
				tree.append(("comment", chunks[0][1:]))
			else:
				raise ValueError("Unrecognized syntax")

			statNum += 1


