class TokensPreprocess(object):

	def __init__(self, out):
		self.out = out

	def Analyse(self, lexData):

		lineNum = 0
		
		while lineNum < len(lexData):

			indent, chunks = lexData[lineNum]
			#print (indent, chunks)

			tokenIndex = None
			for i, tok in enumerate(chunks):
				if len(tok) > 0 and tok[0] == '#':
					tokenIndex = i

			if tokenIndex is None or tokenIndex == 0:
				self.out.append((lineNum, indent, chunks))

			else:
				#Split comment into two lines
				self.out.append((lineNum, indent, chunks[:tokenIndex]))

				self.out.append((lineNum, indent, chunks[tokenIndex:]))

			lineNum += 1

