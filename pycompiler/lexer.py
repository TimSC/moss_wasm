

class Lexer(object):

	def __init__(self, out):
		self.singleCharEnts = ['(', ')', '{', '}', '[', ']', ':', ',']
		self.operatorChars = ['=', '<', '>', '!', '+', '-', '*', '/']
		self.breakChars = ['\n', '\t', '\r', ' ']
		self.commentChar = ['#']
		self.lineChunks = []
		self.inComment = False
		self.indents = 0
		self.out = out

	def Parse(self, fi):

		buff = []
		for ch in fi.read():

			if not self.inComment:

				#End chunk if encountering any singleCharEnts or breakChars
				if ch in self.singleCharEnts + self.breakChars + self.commentChar:
					
					if len(buff) > 0:
						self.Chunk("".join(buff))
						buff=[]

				elif len(buff) > 0:

					#Switching between normal characters and operator chars ends the chunk
					prevOperatorChar = buff[-1] in self.operatorChars
					isOperatorChar = ch in self.operatorChars
									
					if prevOperatorChar != isOperatorChar:
						self.Chunk("".join(buff))
						buff=[]

				if ch in self.singleCharEnts:
					#Single char operators are simple to process
					self.Chunk(ch)
				elif ch not in self.breakChars:
					buff.append(ch)

				if len(self.lineChunks)==0 and ch == "\t":
					self.indents += 1

			else:
				if ch != '\n':
					#Character in comment
					buff.append(ch)
				else:
					#End of line is end of comment
					self.Chunk("".join(buff))
					buff=[]
					self.inComment = False

			if ch == '#':
				self.inComment = True

			#End of line is often the end of a statement
			if ch =="\n":
				self.EndStatement()

		if len(buff) > 0:
			self.Chunk("".join(buff))

	def Chunk(self, found):
		
		self.lineChunks.append(found)

	def EndStatement(self):

		self.out.append((self.indents, self.lineChunks))
		self.lineChunks = []
		self.indents = 0

